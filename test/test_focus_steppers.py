import time
# import pigpio
# from TMC_2209.TMC_2209_StepperDriver import *
from TMC_2209 import TMC_2209_StepperDriver as tmc

motors = {'primary': {'en_pin': 27, 'address': 0},
          'secondary': {'en_pin': 22, 'address': 1},
          'focus': {'en_pin': 19, 'address': 2}
          }
motor = 'focus'
fs = tmc.TMC_2209(pin_en = motors[motor]['en_pin'], driver_address = motors[motor]['address'], no_uart = False, loglevel = tmc.Loglevel.DEBUG)

fs.set_current(1000)
fs.set_interpolation(True)
fs.set_spreadcycle(True)
fs.set_microstepping_resolution(256)
fs.set_internal_rsense(False)
# fs.set_acceleration_fullstep(1000)
# fs.set_max_speed_fullstep(250)

fs.readIOIN()
fs.readCHOPCONF()
fs.readDRVSTATUS()
fs.readGCONF()

# fs.set_movement_abs_rel(tmc.MovementAbsRel.ABSOLUTE)
fs.set_motor_enabled(True)
# fs.run_to_position_steps(400)
fs.set_direction_reg(True)
fs.set_vactual_rpm(rpm=100, revolutions=0.75)
time.sleep(2)
fs.set_direction_reg(False)
fs.set_vactual_rpm(rpm=10, revolutions=0.75)
# fs.run_to_position_steps(0)
fs.set_motor_enabled(False)

fs.deinit()
