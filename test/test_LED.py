import time
import pigpio

LED_PIN = 18

pio = pigpio.pi()

for i in range(0,2*8):
    pio.set_PWM_dutycycle(LED_PIN, 2**(i%8))
    time.sleep(1)
    
pio.set_PWM_dutycycle(LED_PIN, 0)

