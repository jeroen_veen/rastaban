from PyQt5.QtCore import QTimer, QEventLoop, pyqtSignal


def wait_ms(timeout: int):
    """ Block loop until timeout (ms) elapses.
    :param timeout: ms before timeout
    """
    loop = QEventLoop()
    QTimer.singleShot(timeout, loop.exit)
    loop.exec_()


def wait_signal(signal: pyqtSignal, timeout: int = 1000) -> int:
    """ Block loop until signal received, or until timeout (ms) elapsed.
    :param signal: pyqtSignal to wait for
    :param timeout: ms before timeout
    :return: 0 on signal received, -1 on time out
    """
    loop = QEventLoop()
    signal.connect(lambda: loop.exit(0))  # success, return 0
    QTimer.singleShot(timeout, lambda: loop.exit(-1))  # time out, return -1
    ret = loop.exec_()

    return ret
