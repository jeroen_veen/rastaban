from PyQt5.QtCore import QTimer, QObject, pyqtSignal, pyqtSlot
import psutil


class CheckResources(QObject):
    timer = QTimer()
    postMessage = pyqtSignal(str)

    def __init__(self, interval=100):
        super().__init__()

        self.interval = 1000 * interval
        self.timer.timeout.connect(self.update)
        self.timer.start(self.interval)

    def update(self):
        try:
            cpu = psutil.cpu_percent(percpu=True)
            memory = psutil.virtual_memory().percent
            swap = psutil.swap_memory().percent
            cpu_thermal = psutil.sensors_temperatures()['cpu_thermal'][0].current            
#             available = round(memory.available / 1024.0 / 1024.0, 1)
#             total = round(memory.total / 1024.0 / 1024.0, 1)
            info_str = f"{self.__class__.__name__}: info; cpu={str(cpu)}%, " 
            info_str += f"memory={memory:.1f}%, swap={swap:.1f}%, cpu_thermal={cpu_thermal:.1f}" 
            self.postMessage.emit(info_str)

        except Exception as err:
            self.postMessage.emit(f"{self.__class__.__name__}: error; type: {type(err)}, args: {err.args}")

    @pyqtSlot()
    def stop(self):
        self.postMessage.emit(f"{self.__class__.__name__}: info; stopping")
        if self.timer.isActive():
            self.timer.stop()


if __name__ == "__main__":
    from PyQt5.QtCore import QCoreApplication
    from testMessageSink import TestMessageSink

    app = QCoreApplication([])

    cr = CheckResources(20)
    tm = TestMessageSink()

    cr.postMessage.connect(tm.receive_message)

    app.exec_()
