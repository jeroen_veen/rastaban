import subprocess, pigpio, time

def start_pigpiod() -> bool:
    """
    Start pigpio daemon if it is not running already
    :return: True on success, false on failure
    """
    # check if daemon is running 
    status, process = subprocess.getstatusoutput('sudo pidof pigpiod')

    if status:
        print("pigpiod was not running")
        subprocess.getstatusoutput('sudo pigpiod')  # try to  start it
        time.sleep(0.5)
        # check it again        
        status, process = subprocess.getstatusoutput('sudo pidof pigpiod')

    if not status:  # if it was started successfully (or was already running)...
        pigpiod_process = process
        print(f"pigpiod is running, process ID is {pigpiod_process}")
        return True
    
    return False
