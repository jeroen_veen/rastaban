from subprocess import check_output


def check_camera():
    """
    Returns true if a supported raspberrypi camera is detected.
    Note that this doesn't work with Bullseye Raspberry Pi OS!
    Probably because we use the legacy driver
    """
    try:
        res = check_output(['/opt/vc/bin/vcgencmd', 'get_camera']).decode('UTF-8')
        supported_str, detected_str = res.split()

        supported = supported_str.split('=')[-1] == '1'
        detected = detected_str.split('=')[-1] == '1'

        return supported & detected
    except Exception as err:
        print(f"check_camera: error; type: {type(err)}, args: {err.args}")
    return False
