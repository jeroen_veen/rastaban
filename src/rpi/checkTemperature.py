from PyQt5.QtCore import QTimer, QObject, pyqtSlot, pyqtSignal
from subprocess import check_output


def get_gpu_temp():
    ## Return GPU temperature as a string, based on https://github.com/gavinlyonsrepo/raspberrypi_tempmon
    #     a some point os.popen keeps throwing an OSError: [Errno 12] Cannot allocate memory
    # no clue how to solve this, so just skip GPU temperature
    """ Return GPU temperature as a character string"""
    res = check_output(['/opt/vc/bin/vcgencmd', 'measure_temp']).decode('UTF-8')
    res = res.replace("temp=", "")
    res = res.replace("'C", "")
    return res


class CheckTemperature(QObject):
    postMessage = pyqtSignal(str)
    timer = QTimer()
    alarm = pyqtSignal()
    alarmRemoved = pyqtSignal()
    failure = pyqtSignal()

    def __init__(self, interval=1, alarm_temperature=50, failure_temperature=75):
        super().__init__()
        self.interval = 1000 * interval
        self.threshold = alarm_temperature
        self.fail_threshold = failure_temperature
        self.timer.timeout.connect(self.update)
        self.timer.start(self.interval)
        self.alarmed = False
        self.temperature = None

    def update(self):
        try:
            cpu_temp = float(self.get_cpu_temp())
            self.temperature = cpu_temp
            if cpu_temp > self.fail_threshold:
                self.failure.emit()
                self.postMessage.emit(
                    '{}: error; T_CPU={:.1f}°C, temperature gets too high'.format(self.__class__.__name__, cpu_temp))
            elif (cpu_temp > self.threshold) and not self.alarmed:
                self.alarm.emit()
                self.alarmed = True
                self.postMessage.emit(
                    '{}: info; T_CPU={:.1f}°C, temperature alarm on'.format(self.__class__.__name__, cpu_temp))
            elif (cpu_temp < 0.95 * self.threshold) and self.alarmed:
                self.alarmRemoved.emit()
                self.alarmed = False
                self.postMessage.emit(
                    '{}: info; T_CPU={:.1f}°C, temperature alarm off'.format(self.__class__.__name__, cpu_temp))
        except Exception as err:
            self.postMessage.emit(f"{self.__class__.__name__}: error; type: {type(err)}, args: {err.args}")

    @pyqtSlot()
    def reset_alarm(self):
        self.alarmRemoved.emit()
        self.alarmed = False

    @pyqtSlot()
    def stop(self):
        self.postMessage.emit("{}: info; stopping".format(self.__class__.__name__))
        if self.timer.isActive():
            self.timer.stop()

    def get_cpu_temp(self):
        """ Return CPU temperature """
        result = 0
        try:
            my_path = "/sys/class/thermal/thermal_zone0/temp"
            with open(my_path, 'r') as mytmpfile:
                for line in mytmpfile:
                    result = line
            result = float(result) / 1000
            result = round(result, 1)
        except Exception as err:
            self.postMessage.emit(f"{self.__class__.__name__}: error; type: {type(err)}, args: {err.args}")
        return str(result)
