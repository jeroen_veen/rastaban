import pigpio
from PyQt5.QtCore import QObject, QMutex, QSettings, pyqtSignal, pyqtSlot


class LightSourceControl(QObject):
    postMessage = pyqtSignal(str)

    pio = None  # Reference to PigPIO object
#     mutex = QMutex()

    def __init__(self, pio, my_settings: QSettings):
        super().__init__()

        if not isinstance(pio, pigpio.pi):
            raise TypeError("Constructor attribute is not a pigpio.pi instance!")
        self.pio = pio
        self.led_pin = my_settings.value('light_source/led_pin', None, int)

    @pyqtSlot(int)
    def set_val(self, val):
        """ Set PWM dutycycle.
            :param val: PWM dutycycle, int between 0 and 100
        """       
        value = int(round(255 * float(val) / 100))
        try:
            if self.pio is not None:
                self.pio.set_PWM_dutycycle(self.led_pin, value)
        except Exception as err:
            self.postMessage.emit(f"{self.__class__.__name__}: error; type: {type(err)}, args: {err.args}")

    @pyqtSlot()
    def stop(self):
        try:
            self.set_val(0)
        except Exception as err:
            self.postMessage.emit(f"{self.__class__.__name__}: error; type: {type(err)}, args: {err.args}")

