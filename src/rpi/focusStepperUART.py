from PyQt5.QtCore import QObject, QThread, QMutex, QSettings, pyqtSignal, pyqtSlot
from TMC_2209 import TMC_2209_StepperDriver as tmc
from wait import wait_ms
from time import time


class FocusStepper(QThread):
    postMessage = pyqtSignal(str)
    
    def __init__(self, my_settings: QSettings):
        super().__init__()
        
        if not('motor' in my_settings.value('focus_control/actuator', None, str)):
            self.postMessage.emit(f"{self.__class__.__name__}: error; This class expects motor settings")

        self.not_en_pin = my_settings.value('focus_control/motor_not_en_pin', None, int)
        self.step_pin = my_settings.value('focus_control/motor_step_pin', None, int)
        self.dir_pin = my_settings.value('focus_control/motor_dir_pin', None, int)
        self.address = my_settings.value('focus_control/motor_address', None, int)
        self.current = my_settings.value('focus_control/motor_current', None, int)
        self.motor_microsteps = my_settings.value('focus_control/motor_microsteps', None, int)
        self.rpm = my_settings.value('focus_control/motor_rpm', None, int)        
        self.acceleration_fullstep = my_settings.value('focus_control/motor_acceleration_fullstep', None, int)
        self.max_speed_fullstep = my_settings.value('focus_control/motor_max_speed_fullstep', None, int)
        self.tmc = tmc.TMC_2209(pin_en = self.not_en_pin, pin_step = self.step_pin, pin_dir = self.dir_pin, driver_address = self.address, no_uart = False) #, loglevel = tmc.Loglevel.DEBUG)
#         self.tmc = tmc.TMC_2209(pin_en = self.not_en_pin, driver_address = self.address, no_uart = False) #, loglevel = tmc.Loglevel.DEBUG)
        self.tmc.set_direction_reg(False)
        self.tmc.set_current(self.current)
        self.tmc.set_interpolation(True)
#         self.tmc.set_spreadcycle(True)
        self.tmc.set_microstepping_resolution(self.motor_microsteps)
        self.tmc.set_internal_rsense(False)
        self.tmc.set_acceleration_fullstep(self.acceleration_fullstep)
        self.tmc.set_max_speed_fullstep(self.max_speed_fullstep)
        self.tmc.readIOIN()
        self.tmc.readCHOPCONF()
        self.tmc.readDRVSTATUS()
        self.tmc.readGCONF()
        self.tmc.set_movement_abs_rel(tmc.MovementAbsRel.ABSOLUTE)
        self.movement_abs_rel = True
        self.current_position = 0
    
    @pyqtSlot()
    def go_home(self):
        self.move(-1)
        
    @pyqtSlot(int)
    def set_abs_rel_movement(self, val:int):
        """ Set absolute or relative stepper movement.
            :param val: steps
        """
        self.movement_abs_rel = val
        if val:
            self.tmc.set_movement_abs_rel(tmc.MovementAbsRel.ABSOLUTE)
            self.postMessage.emit(f"{self.__class__.__name__}: info; absolute movement")
        else:
            self.tmc.set_movement_abs_rel(tmc.MovementAbsRel.RELATIVE)    
            self.postMessage.emit(f"{self.__class__.__name__}: info; relative movement")
        
    @pyqtSlot(int)
    def set_position(self, steps:int):
        """ Set stepper position from home by nr of steps.
            :param val: steps
        """
        # STEP/DIR/UART control
        self.tmc.set_motor_enabled(True)
        self.tmc.run_to_position_steps(steps)
        self.tmc.set_motor_enabled(False)
        
#         # UART only control
# This amounts to velocity control, see datasheet, quite inaccurqte for position
#         position = float(steps) / 1000.0        
# #         if position < 0:
# #             self.postMessage.emit("invalid focus stepper position")
# #             return        
#         revolutions = position - self.current_position
#         print(revolutions)
#         print(revolutions, position, self.current_position)
#         if abs(revolutions) > 1e-3:
#             self.move(revolutions)
#             self.current_position = position

    @pyqtSlot(int)
    def move(self, revolutions:float):
        """ Move by nr of revolutions.
            :param val: revolutions
        """
        try:
            revolutions = -revolutions  # seems to negated wrt to current wiring
            self.tmc.set_motor_enabled(True)
            self.tmc.set_vactual_rpm(rpm=self.rpm, revolutions=revolutions)
            self.tmc.set_motor_enabled(False)
        except Exception as err:
            self.postMessage.emit(f"{self.__class__.__name__}: error; type: {type(err)}, args: {err.args}")

    @pyqtSlot()
    def stop(self):
        try:
            if self.movement_abs_rel:
                self.set_position(0)
            self.tmc.deinit()
        except Exception as err:
            self.postMessage.emit(f"{self.__class__.__name__}: error; type: {type(err)}, args: {err.args}")

