import pigpio
from PyQt5.QtCore import QObject, QSettings, pyqtSignal, pyqtSlot


class VoiceCoil(QObject):
    postMessage = pyqtSignal(str)

    pio = None  # Reference to PigPIO object

    def __init__(self, pio, my_settings: QSettings):
        super().__init__()
        if not isinstance(pio, pigpio.pi):
            raise TypeError("Constructor attribute is not a pigpio.pi instance!")
        self.pio = pio
        self.dir_pin = my_settings.value('focus_control/vc_dir_pin', None, int)
        self.pwm_pin = my_settings.value('focus_control/vc_pwm_pin', None, int)
        self.pwm_frequency = my_settings.value('focus_control/vc_pwm_frequency', None, int)
        self.pio.set_mode(self.dir_pin, pigpio.OUTPUT)
        self.pio.hardware_PWM(self.pwm_pin, self.pwm_frequency, 0)

    @pyqtSlot(float)
    def set_val(self, val):
        """ Set voicecoil output pin to PWM value.
            :param val: PWM dutycycle, float between -100 and 100, polarity defines direction
        """
        value = int(val * 1e4)
        try:
            if self.pio is not None:
                self.pio.hardware_PWM(self.pwm_pin, self.pwm_frequency, abs(value))
                self.pio.write(self.dir_pin, value > 0)
        except Exception as err:
            self.postMessage.emit(f"{self.__class__.__name__}: error; type: {type(err)}, args: {err.args}")

    @pyqtSlot()
    def stop(self):
        try:
            if self.pio is not None:
                self.pio.hardware_PWM(self.pwm_pin, 0, 0)  # PWM off
        except Exception as err:
            self.postMessage.emit(f"{self.__class__.__name__}: error; type: {type(err)}, args: {err.args}")
