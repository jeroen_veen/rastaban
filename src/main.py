from os import sep, remove
from glob import glob
import sys

# sys.path.append("rpi")

from PyQt5.QtCore import Qt, QSettings
from PyQt5.QtWidgets import QApplication

from rpi.checkConnection import CheckInternet
from rpi.checkOS import is_raspberry_pi
# from rpi.checkTemperature import CheckTemperature
from rpi.voicecoil import VoiceCoil
from rpi.focusStepperUART import FocusStepper
from rpi.lightSourceControl import LightSourceControl
from checkResources import CheckResources
from mainWindow import MainWindow
from rpi.PyQtPiCam import PiVideoStream
from rpi.start_pigpiod import start_pigpiod
from log import LogWindow
from wait import wait_ms
import qdarkstyle
import pigpio


CFG_PATH = "cfg"

if __name__ == '__main__':
    if not is_raspberry_pi():
        print("ERROR: This app is for raspberrypi")
        exit()
    start_pigpiod()
    pio = pigpio.pi()
    if not pio.connected:
        print("ERROR: pigpio daemon error")
        exit()

    settings_filename = sep.join([CFG_PATH, 'settings.ini'])
    settings = QSettings(settings_filename, QSettings.IniFormat)

    files = glob(sep.join([settings.value('temp_folder', None, type=str), '*.*']))
    for file in files:
        remove(file)

    # Create event loop and instantiate objects
    app = QApplication(sys.argv)
    dark_stylesheet = qdarkstyle.load_stylesheet_pyqt5()
    app.setStyleSheet(dark_stylesheet)

    mw = MainWindow(settings)
    lw = LogWindow(settings)
    vs = PiVideoStream(settings)
#     st = CheckTemperature(interval=10, alarm_temperature=55)
    cf = CheckInternet()
    cr = CheckResources(600)
    vc = VoiceCoil(pio, settings)
    fs = FocusStepper(settings)
    ls = LightSourceControl(pio, settings)

    # Connect logging signals
    vs.postMessage.connect(lw.append)
#     st.postMessage.connect(lw.append)
    cf.postMessage.connect(lw.append)
    cr.postMessage.connect(lw.append)
    vc.postMessage.connect(lw.append)
    fs.postMessage.connect(lw.append)
    ls.postMessage.connect(lw.append)

    # Connect functional signals
    vs.frame.connect(mw.update, type=Qt.BlockingQueuedConnection)
    mw.snapshotButton.clicked.connect(lambda: vs.take_image())
    mw.recordButton.clicked.connect(lambda: vs.record_clip(None, duration=30))
    mw.runButton.released.connect(mw.disableButtons)
    mw.VCSpinBox.valueChanged.connect(vc.set_val)
    mw.light.valueChanged.connect(ls.set_val)
#     mw.focusUpButton.pressed.connect(lambda: fs.move(50))
#     mw.focusDownButton.pressed.connect(lambda: fs.move(-50))
#     mw.focusUpButton.released.connect(lambda: fs.move(0))
#     mw.focusDownButton.released.connect(lambda: fs.move(0))
    mw.homeButton.clicked.connect(fs.go_home)
    mw.FocusStepperSpinBox.valueChanged.connect(fs.set_position, type=Qt.DirectConnection)
    mw.AbsoluteCheckBox.stateChanged.connect(fs.set_abs_rel_movement)

    # Connect closing signals
    mw.closed.connect(lw.close, type=Qt.QueuedConnection)
    mw.closed.connect(ls.stop, type=Qt.QueuedConnection)
    mw.closed.connect(fs.stop, type=Qt.QueuedConnection)
#     st.failure.connect(lw.close, type=Qt.QueuedConnection)
#     lw.closed.connect(st.stop, type=Qt.QueuedConnection)
    lw.closed.connect(cf.stop, type=Qt.QueuedConnection)
    lw.closed.connect(vs.stop, type=Qt.QueuedConnection)
    lw.closed.connect(mw.close, type=Qt.QueuedConnection)
    lw.closed.connect(vc.stop)

    # Start the show
    mw.move(100, 100)
    mw.resize(1500, 500)
    lw.move(100, 800)
    lw.resize(1500, 200)
    mw.show()
    lw.show()

    # Start-up recipe
    vs.init_stream()
    vc.set_val(mw.VCSpinBox.value())

    app.exec_()
